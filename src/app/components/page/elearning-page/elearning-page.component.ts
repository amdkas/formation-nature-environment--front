import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-elearning-page',
  templateUrl: './elearning-page.component.html',
  styleUrls: ['./elearning-page.component.css']
})
export class ElearningPageComponent implements OnInit {

  public urlSafe: SafeResourceUrl;
  public isLoaded: Boolean = false;

  constructor(
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.urlSafe = this._sanitizer.bypassSecurityTrustResourceUrl(localStorage.getItem("elearning"));
  }

  public loadEvent(): void {
    this.isLoaded = true;
  }
}