import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DialogItemComponent } from '../dialog-item/dialog-item.component';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { Question } from 'src/app/services/game/question.model';
import { Response } from 'src/app/services/game/response.model';

@Component({
  selector: 'app-question-displayer-item',
  templateUrl: './question-displayer-item.component.html',
  styleUrls: ['./question-displayer-item.component.css']
})
export class QuestionDisplayerItemComponent {

  @Input() public questionInformation: Question = null;
  @Output() public answer = new EventEmitter<Response>();

  public url: string = `${environment.backend}`;

  constructor(
    private _dialog: MatDialog
  ) { }

  public verify(answer: Response): void {
    this.answer.emit(answer);
  }

  public openDialog(): void {
    this._dialog.open(DialogItemComponent, {
      width: '80%',
      height: '80%',
      data: this.url + this.questionInformation.situationImage
    });
  }
}
