import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-item',
  template: `
    <div fxLayoutAlign="center center">
      <img class="img-formatter" src="{{ url }}" alt="" />
    </div>
  `,
  styles: [` .img-formatter { width: 100%; } `]
})
export class DialogItemComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public url: String
  ) { }
}
