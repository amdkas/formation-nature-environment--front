import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './components/page/home-page/home-page.component';
import { ElearningPageComponent } from './components/page/elearning-page/elearning-page.component';
import { WikiPageComponent } from './components/page/wiki-page/wiki-page.component';
import { QuizzPageComponent } from './components/page/quizz-page/quizz-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'e-learning', component: ElearningPageComponent },
  { path: 'wiki', component: WikiPageComponent },
  { path: 'quizz', component: QuizzPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
