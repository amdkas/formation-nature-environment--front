import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from "@angular/flex-layout";

import { MenuItemComponent } from './components/item/menu-item/menu-item.component';
import { LoaderItemComponent } from './components/item/loader-item/loader-item.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { YouTubePlayerModule } from "@angular/youtube-player";

import { HomePageComponent } from './components/page/home-page/home-page.component';
import { ElearningPageComponent } from './components/page/elearning-page/elearning-page.component';
import { QuestionDisplayerItemComponent } from './components/item/question-displayer-item/question-displayer-item.component';
import { DialogItemComponent } from './components/item/dialog-item/dialog-item.component';
import { WikiPageComponent } from './components/page/wiki-page/wiki-page.component';
import { QuizzPageComponent } from './components/page/quizz-page/quizz-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuItemComponent,
    ElearningPageComponent,
    HomePageComponent,
    LoaderItemComponent,
    QuestionDisplayerItemComponent,
    DialogItemComponent,
    WikiPageComponent,
    QuizzPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    NgxAudioPlayerModule,
    YouTubePlayerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
