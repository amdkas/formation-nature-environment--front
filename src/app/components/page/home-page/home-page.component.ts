import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public logoPath: string = `${environment.backend}img/SGDF_logo_RVB_horizontal__NE.png`;
  public blasonPath: string = `${environment.backend}img/SGDF_nature_environnement.png`;
  public pathPath: string = `${environment.backend}img/chemin.png`;

  public site: string;
  public elearning: string;
  public wiki: string;

  constructor() { }

  ngOnInit(): void {
    this.site = localStorage.getItem("site");
    this.elearning = localStorage.getItem("elearning");
    this.wiki = localStorage.getItem("wiki");
  }
}
