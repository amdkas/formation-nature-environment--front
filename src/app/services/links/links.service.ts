import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Links } from './links.model';

@Injectable({
  providedIn: 'root'
})
export class LinksService {

  private _url: string;

  constructor(
    private _httpService: HttpClient
  ) {
    this._url = `${environment.backend}links/`
  }

  public getLinks(): Observable<Links> {
    return this._httpService.get<Links>(`${this._url}links.json`);
  }
}
