export class Question {
    public situationImage: string;
    public videoUrl: string;
    public situationRecord: string;
    public question: string;
    public explication: string;
    public possibleAnswers: Response[];
}