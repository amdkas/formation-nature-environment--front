import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-wiki-page',
  templateUrl: './wiki-page.component.html',
  styleUrls: ['./wiki-page.component.css']
})
export class WikiPageComponent implements OnInit {

  public urlSafe: SafeResourceUrl;
  public isLoaded: Boolean = false;

  constructor(
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.urlSafe = this._sanitizer.bypassSecurityTrustResourceUrl(localStorage.getItem("wiki"));
  }

  public loadEvent(): void {
    this.isLoaded = true;
  }
}
