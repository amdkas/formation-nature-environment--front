# Curriculum Vitae Front

## This repository

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.2.
This repository presents your professionnal life with an awesome design.

## Documentation

If you want to know more about this project you can have a look on the [documentation](documentation/NE-Documentation_fonctionnelle.pdf).

## How to install and use it ?

1. Install [Node.js](https://nodejs.org/en/download/)
2. Install [Angular CLI](https://cli.angular.io/)
3. Clone this repo
4. In the project folder (example: ~/git/curriculum-vitae--front),
   run `npm install` | `npm i`

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).   

#### Run the project with development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Deploy your project with GitHub Page

Run `ng build --prod --output-path docs --base-href /formation-nature-environment--front/` to build the application in production in the root folder.

If you need more information you can check the [GitHub Page Documentation](https://docs.github.com/en/free-pro-team@latest/github/working-with-github-pages) and the [Angular Documentation](https://angular.io/guide/deployment#fallback-configuration-examples)

## Contributing

* Create an issue and explain your problem
* Fork it / Create your branch / Commit your changes / Push to your branch / Submit a pull request

## Maintainer

Luc AUCOIN <luc.aucoin1998@gmail.com>