import { Component, OnInit } from '@angular/core';
import { LinksService } from './services/links/links.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // Is the sidebar opened
  public isOpened: boolean;

  constructor (
    private _linksService: LinksService
  ) {}

  ngOnInit(): void {
    this._getLinks();
  }

  private _getLinks(): void {
    this._linksService.getLinks().subscribe(data => {
      localStorage.setItem("site", data.site);
      localStorage.setItem("elearning", data.elearning);
      localStorage.setItem("wiki", data.wiki);
    }, error => {
      console.log(error);
    })
  }
}
