import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Question } from 'src/app/services/game/question.model';
import { MapService } from 'src/app/services/game/map/map.service';
import { DfciService } from 'src/app/services/game/dfci/dfci.service';
import { ForestService } from 'src/app/services/game/forest/forest.service';
import { RadioService } from 'src/app/services/game/radio/radio.service';
import { Response } from 'src/app/services/game/response.model';

@Component({
  selector: 'app-quizz-page',
  templateUrl: './quizz-page.component.html',
  styleUrls: ['./quizz-page.component.css']
})
export class QuizzPageComponent implements OnInit {

  public url: string = `${environment.backend}`;

  public proposedQuizz: Question[] = [];
  public resultQuizz: Response[] = [];

  public questionInformation: Question;
  public answer: Response;
  public score: number = 0;

  public actualIndex: number = 0;
  public isFinished = false;

  public endSentence: string;

  constructor(
    private route: ActivatedRoute,
    private mapService: MapService,
    private dfciService: DfciService,
    private forestService: ForestService,
    private radioService: RadioService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.isFinished = false;
      this.score = 0;
      this.actualIndex = 0;
      switch(params["categorie"]){
        case 'dfci':
          this._getDfciQuestions();
          break;
        case 'environnement-mediterranee':
          this._getForestQuestions();
          break;
        case 'radio':
          this._getRadioQuestions();
          break;
        default:
          this._getMapQuestions();
          break;
      }
    }, error => {
      console.log(error);
    });
  }

  private _getMapQuestions(): void {
    this.mapService.getMapQuestions().subscribe(data => {
      if(data?.length) {
        this.proposedQuizz = this._getRandomNumbers(data);
        this._next();
      }
    }, error => {
      console.log(error);
    });
  }

  private _getDfciQuestions(): void {
    this.dfciService.getDfciQuestions().subscribe(data => {
      if(data?.length) {
        this.proposedQuizz = this._getRandomNumbers(data);
        this._next();
      }
    }, error => {
      console.log(error);
    });
  }

  private _getForestQuestions(): void {
    this.forestService.getForestQuestions().subscribe(data => {
      if(data?.length) {
        this.proposedQuizz = this._getRandomNumbers(data);
        this._next();
      }
    }, error => {
      console.log(error);
    });
  }

  private _getRadioQuestions(): void {
    this.radioService.getRadioQuestions().subscribe(data => {
      if(data?.length) {
        this.proposedQuizz = this._getRandomNumbers(data);
        this._next();
      }
    }, error => {
      console.log(error);
    });
  }

  public isContinue({ answer }: { answer: Response }): void {
    this.resultQuizz[this.actualIndex] = answer;    
    this.actualIndex++
    
    if(answer.isCorrect)
      this.score++;
      
    if(this.proposedQuizz?.length > this.actualIndex)
      this._next();
    else {
      var successPercent: number;
      successPercent = this.score/this.proposedQuizz.length;

      if(successPercent >= 0.80)
        this.endSentence = "Tu as passé ce test avec brio ! Mais qui est brio ?";
      else if(successPercent < 0.80 && successPercent >= 0.50)
        this.endSentence = "N'hésites pas à recommencer ce test pour prendre de l'assurance 😉 !"
      else
        this.endSentence = "Oupsi ! Tu peux aller faire un tour sur le e-learning 😉. Ça te permettra d'apprendre plein de choses cools et utiles !"

      this.isFinished = true;
    }
  }

  private _next(): void {
    this.questionInformation = this.proposedQuizz[this.actualIndex];
    this.questionInformation.possibleAnswers.sort(() => Math.random() - 0.5)
  }

  protected _getRandomNumbers(data: Question[]): Question[] {
    var arr: number[] = [];
    var size: number = 10;

    var proposedQuizz: Question[] = []

    if(data.length <= size) {
        return data.sort(() => Math.random() - 0.5);
    }
    else {
        while(arr.length < size){
            var r = Math.floor(Math.random() * data?.length);
            if(arr.indexOf(r) === -1) arr.push(r);
        }

        for(var i=0; i<size; i++)
            proposedQuizz.push(data[arr[i]]);

        return proposedQuizz;
    }
}

}
