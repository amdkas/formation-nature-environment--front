import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader-item',
  template: '<mat-progress-spinner class="custom-spinner" mode="indeterminate" diameter="40"></mat-progress-spinner>'
})
export class LoaderItemComponent {}
