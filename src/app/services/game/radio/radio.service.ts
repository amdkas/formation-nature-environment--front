import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Question } from '../question.model';

@Injectable({
  providedIn: 'root'
})
export class RadioService {

  private _url: string;

  constructor(
    private _httpService: HttpClient
  ) {
    this._url = `${environment.backend}game/`
  }

  public getRadioQuestions(): Observable<Question[]> {
    return this._httpService.get<Question[]>(`${this._url}radio.json`);
  }
}
